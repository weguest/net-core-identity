﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using projeto.identity.infra.identity.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace projeto.identity.infra.identity.Services
{
    public interface IUserService
    {
        IdentityResult<IdentityUser> Register(string login, string password, string role);
        IdentityResult<object> Login(string login, string password);
        IdentityResult<object> Funcao(string id, string funcao);
        IdentityResult<object> FuncaoListar(string id);
        void SeedAdmin();
    }
}
