﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using projeto.identity.infra.identity.Services;

namespace projeto.identity.presentation.usuarios.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SegurancaController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger _logger;
        public SegurancaController(IUserService userService, ILogger<SegurancaController> logger)
        {
            _userService = userService;
            _logger = logger;
        }


        #region Autorizacao
        // GET api/values
        [HttpPost("Login")]
        [AllowAnonymous]
        public ActionResult<IEnumerable<string>> Login(LoginViewModel model)
        {
            var token = _userService.Login(model.Login, model.Password);
            if (token.Succeeded == true)
            {
                return Ok(token);
            }
            else
            {
                return Unauthorized();
            }
        }
        #endregion

        #region Registro
        // GET api/values
        [HttpPost("Registro")]
        [Authorize]
        public ActionResult<IEnumerable<string>> Registro(LoginViewModel model)
        {
            var token = _userService.Register(model.Login, model.Password, "User");
            if (token.Succeeded == true)
            {
                return Ok(token);
            }
            else
            {
                return Unauthorized();
            }
        }

        #endregion

        #region usuario
        [HttpPost("usuario/{id}/Funcao/{funcao}")]
        [Authorize]
        public ActionResult<IEnumerable<string>> Funcao([FromRoute] string id, [FromRoute] string funcao)
        {
            var token = _userService.Funcao(id, funcao);
            if (token.Succeeded == true)
            {
                return Ok(token);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("usuario/{id}/FuncaoListar")]
        [Authorize]
        public ActionResult<IEnumerable<string>> FuncaoListar([FromRoute] string id)
        {
            var retorno = _userService.FuncaoListar(id);
            if (retorno.Succeeded == true)
            {
                return Ok(retorno);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("usuario/{id}/FuncaoAdmin")]
        [Authorize]
        public ActionResult<IEnumerable<string>> FuncaoAdmin([FromRoute] string id)
        {

            var retorno = _userService.FuncaoListar(id);
            if (retorno.Succeeded == true)
            {
                return Ok(retorno);
            }
            else
            {
                return Unauthorized();
            }
        }
        #endregion
    }

    public class LoginViewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }

}